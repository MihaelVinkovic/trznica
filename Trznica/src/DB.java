/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mihael
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DB {
    public java.sql.Connection connection;
    public Logger logger;
    public Logger logger_err;
    
    
    public DB(){
        try{
            
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public boolean isConnected(){
        if(this.connection != null){
            return true;
        }else{
            return false;
        }
    }
    
    public boolean disconnect() throws SQLException{
        boolean ok = false;
        try{
            if(connection != null){
                connection.close();
                ok = true;
            }
        }
        catch(SQLException e){
            logger_err.log(Level.WARNING,"SQLExceptione: " + e.getMessage());
        }finally{
            return ok;
        }
    }
    
    public void Connect_MySql(){
            try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            /*     DriverManager.getConnection("jdbc:mysql://"server":"port"/"dbname"?user="user"&password="password"&characterEncoding=Cp1250");
             */
            java.util.Properties props = new java.util.Properties();

            /*props.put("characterEncoding", "cp1250");
            props.put("characterSetResults", "cp1250");*/
            props.put("user", "root");
            props.put("password", "0000");

            this.connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/trznica", props);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    public boolean UpisOdjela(Odjel data){
        if(this.connection == null){
            return false;
        }
        try{        //provjera ako se odjel s istim nazivom već nalazi u bazi
            if(!OdjelPostoji(data)){
                try{
                    String query = "INSERT INTO odjel(nazivOdjela, stanjeNaOdjelu) VALUES(?,?)";
                    PreparedStatement stmt = this.connection.prepareStatement(query);
                    stmt.setString(1, data.getNaziv());           
                    stmt.setInt(2, data.getStanje());
                    stmt.executeUpdate();
                    stmt.close();

                    return true;            
                }catch(SQLException e){
                    logger_err.log(Level.WARNING,"SQLException: "+ e.getMessage());
                    return false;
                }
        }else{
                return false;
            }
        }catch(SQLException e){
           System.out.println(e.getMessage());
          return false;
        }       
    } 
    
    
    public boolean OdjelPostoji(Odjel odjel)throws java.sql.SQLException{
        try{
            
            Statement stm = this.connection.createStatement();
            String query = "SELECT * FROM odjel WHERE nazivOdjela='"+odjel.getNaziv()+"'";
            ResultSet rs = stm.executeQuery(query);           
            while(rs.next()){
                Odjel o = new Odjel();
                o.setNaziv(rs.getString(2));
                
                if(o.getNaziv().equals(odjel.getNaziv())){
                    return true;
                }
            }
            return false;
        }
        catch(SQLException e){
             System.out.println(e.getMessage());
            return false;
        }
    }   
    
    
    public Vector PrikazOdjelaa()throws java.sql.SQLException{
        Vector vec = new Vector();        
        vec.clear();
        try{
            Statement stm = this.connection.createStatement();
            String query = "SELECT * FROM odjel";
            ResultSet rs = stm.executeQuery(query);
            while(rs.next()){
                Odjel o = new Odjel();
                o.setID(rs.getInt(1));
                o.setNaziv(rs.getString(2));
                o.setStanje(rs.getInt(3));
                vec.add(o);
            }
            stm.close();
            query= null;
            rs.close();
            return vec;            
        }catch(SQLException e){
            logger_err.log(Level.WARNING,"SQLException: " + e.getMessage());
            return vec;
        }
       
    }
    
    public boolean Brisanje(String naziv) throws  java.sql.SQLException{
        if(this.connection== null){return false;}
        try{
            String query = "DELETE FROM odjel WHERE nazivOdjela='"+naziv+"'";
            PreparedStatement stmt = this.connection.prepareStatement(query);
            stmt.executeUpdate();
            stmt.close();
            return true;           
        }catch(SQLException e){
             System.out.println(e.getMessage());
            return false;
        }        
    }
    
    
    
    public boolean UpisProizvoda(Item item){
        if(this.connection == null){ return false; }
        
        try{
            String query = "INSERT INTO lager(nazivOdjela, cijena, kolicinaProizvoda, opis, godinaProizvodnje, zemljaPorijekla) VALUES (?,?,?,?,?,?)";
            PreparedStatement stmt = this.connection.prepareStatement(query);
            stmt.setString(1, item.getNazivOdjela());
            stmt.setFloat(2, item.getCijenaZa1Kg());
            stmt.setInt(3, item.getKolicinaProizvoda());
            stmt.setString(4, item.getOpisProizvoda());
            stmt.setInt(5, item.getGodinaProizvodnje());
            stmt.setString(6, item.getZemljaPorijekla());
            
            stmt.executeUpdate();
            stmt.close();
            
            return true;
            
        }catch(SQLException e){
             System.out.println(e.getMessage());
             return false;
        }
    }
}
