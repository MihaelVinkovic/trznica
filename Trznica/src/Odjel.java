/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mihael
 */
public class Odjel {
    protected int ID;
    protected String naziv="";
    protected int stanje =0;

   
    public Odjel(){
        
    }
    
    
    /**
     * @return the ID
     */
    public int getID() {
        return ID;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(int ID) {
        this.ID = ID;
    }

    /**
     * @return the naziv
     */
    public String getNaziv() {
        return naziv;
    }

    /**
     * @param naziv the naziv to set
     */
    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    /**
     * @return the stanje
     */
    public int getStanje() {
        return stanje;
    }

    /**
     * @param stanje the stanje to set
     */
    public void setStanje(int stanje) {
        this.stanje = stanje;
    }


    
}
