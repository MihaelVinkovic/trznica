/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mihael
 */
public class Item  extends Odjel{
    private int kolicinaProizvoda;
    private String nazivOdjela;
    private int cijenaZa1Kg;
    private String opisProizvoda;
    private int godinaProizvodnje;
    private String zemljaPorijekla;    

    /**
     * @return the cijenaZa1Kg
     */
    public int getCijenaZa1Kg() {
        return cijenaZa1Kg;
    }

    /**
     * @param cijenaZa1Kg the cijenaZa1Kg to set
     */
    public void setCijenaZa1Kg(int cijenaZa1Kg) {
        this.cijenaZa1Kg = cijenaZa1Kg;
    }

    /**
     * @return the opisProizvoda
     */
    public String getOpisProizvoda() {
        return opisProizvoda;
    }

    /**
     * @param opisProizvoda the opisProizvoda to set
     */
    public void setOpisProizvoda(String opisProizvoda) {
        this.opisProizvoda = opisProizvoda;
    }

    /**
     * @return the godinaProizvodnje
     */
    public int getGodinaProizvodnje() {
        return godinaProizvodnje;
    }

    /**
     * @param godinaProizvodnje the godinaProizvodnje to set
     */
    public void setGodinaProizvodnje(int godinaProizvodnje) {
        this.godinaProizvodnje = godinaProizvodnje;
    }

    /**
     * @return the zemljaPorijekla
     */
    public String getZemljaPorijekla() {
        return zemljaPorijekla;
    }

    /**
     * @param zemljaPorijekla the zemljaPorijekla to set
     */
    public void setZemljaPorijekla(String zemljaPorijekla) {
        this.zemljaPorijekla = zemljaPorijekla;
    }

    /**
     * @return the nazivOdjela
     */
    public String getNazivOdjela() {
        return nazivOdjela;
    }

    /**
     * @param nazivOdjela the nazivOdjela to set
     */
    public void setNazivOdjela(String nazivOdjela) {
        this.nazivOdjela = nazivOdjela;
    }

    /**
     * @return the kolicinaProizvoda
     */
    public int getKolicinaProizvoda() {
        return kolicinaProizvoda;
    }

    /**
     * @param kolicinaProizvoda the kolicinaProizvoda to set
     */
    public void setKolicinaProizvoda(int kolicinaProizvoda) {
        this.kolicinaProizvoda = kolicinaProizvoda;
    }

    
    
    
}
